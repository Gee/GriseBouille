```
TITRE = Auteur en grève
TITRE_A = Auteur en grève
TITRE_B = 
PUBLICATION = 2019-12-05
ISBN = (y'en a pas)
PRIX = 0 €
DEPOT_LEGAL = (y'en a pas)
PHOTOGRAPHIE = Gee (CC BY SA)
URL = https://grisebouille.net/auteur-en-greve/
```


## Avant-propos

À l'occasion du mouvement national de grève du 5 décembre 2019 contre
la réforme des retraites en France, je vous propose ce livret qui
rassemble plusieurs de mes textes et bandes dessinées sur la
démocratie, la répartition des richesses, la pensée dominante,
l'effondrement et le rêve…

Aujourd'hui 5 décembre 2019, parce que cette énième attaque contre le
bien commun des classes laborieuses ne fait qu'ajouter une pierre à
l'édifice ignoble de la société capitaliste néolibérale que je conchie
article après article, je suis un auteur en grève.

Vive le socialisme.

Vive l'autogestion.

Vive la sécu.

Bonne lecture et à bientôt sur le _web_ ou ailleurs,

– Gee


# Démocratie & représentation

## Le deuil de la démocratie représentative [deuil]

_Article publié le 7 décembre 2015_

D'abord, un chiffre pour remettre les pendules à l'heure : __91%__.
C'est le pourcentage de français qui n'a pas voté pour le FN[^deuil1]. Moins
d'un français sur 10 a donné une voix à ce parti. Et de fait, que le FN
soit « le premier parti de France » n'est pas en soi le symbole d'une
droitisation ou d'une radicalisation rampante de la société français.
C'est le symbole de la mort de la démocratie représentative, le signe
ultime que celle-ci ne représente plus rien ni personne.

Hier, je n'ai pas voté. Je n'irai pas plus dimanche prochain. Ami
votant[^deuil2], je sais que, probablement, tu me méprises, tu as envie de me
hurler dessus, de me dire que c'est honteux, que des gens sont morts
pour que je puisse voter, qu'à cause de moi le fascisme pourrait
s'installer. Je ne t'en veux pas, j'étais pareil il y a à peine 4 ans.

### Les étapes du deuil

Tu connais peut-être les 5 étapes du deuil de Elisabeth Kübler-Ross. Ça
n'a pas forcément une grande valeur scientifique, mais ça permet de
schématiser certains mécanismes émotionnels. Laisse-moi te les énoncer :

1. Déni

2. Colère

3. Marchandage

4. Dépression

5. Acceptation

Ami votant, je sais déjà que tu as dépassé le stade du déni : tu sais
pertinemment que la démocratie représentative est morte. Sinon, tu
voterais pour des idées qui te correspondent, tu voterais pour faire
avancer la société, pour donner ton avis sur la direction à prendre.
Mais tu ne fais pas cela : au contraire, tu votes « utile », tu votes
pour faire barrage à un parti, tu votes pour « le moins pire ». C'est
déjà un aveu que le système est mort.

En fait, tu oscilles entre les étapes 2 et 3. Entre la colère envers un
système qui se fout de ta gueule, la colère contre les abstentionnistes
qui ne jouent pas le jeu… et le marchandage. « _Allez, si je vote pour
le moins pire, système, tu continues à vivoter ? Allez, peut-être que si
on vote PS cette fois, il fera une vraie politique de gauche ? Allez
système, tu veux pas continuer à faire semblant de marcher un peu si je
fais des concessions de mon côté ? Si je mets mes convictions de côté,
tu veux bien ne pas être totalement lamentable ?_ »

Encore une fois, je comprends le principe, j'étais au même point lors
des dernières élections présidentielles. J'appelais les gens à voter, je
critiquais les abstentionnistes qui se permettaient de se plaindre alors
que, merde, ils n'avaient pas pris la peine de faire leur devoir de
citoyen. Je savais pertinemment que le PS au pouvoir ne ferait aucun
miracle, que fondamentalement rien ne changerait par rapport à l'UMP, à
part à la marge. Mais il fallait bien choisir le moins pire. La
démocratie représentative était déjà morte, je le savais. Le vote utile,
on nous le rabâchait depuis avant même que j'aie le droit de vote. Sans
parler du référendum de 2005 où ça sentait déjà fort le sapin. Mais je
n'avais pas terminé mon deuil. Et puis Hollande est passé.

### Les derniers coups de pelle

Je ne pourrais jamais assez remercier François Hollande. Il m'a aidé à
terminer mon deuil. En me renvoyant ma voix en pleine figure, en
m'appuyant bien profondément la tête dans les restes puants et
décomposés de notre système politique. Le quinquennat de François
Hollande aura été la plus parfaite, la plus magnifique démonstration
que le vote est une arnaque et que le pouvoir du peuple est une
immense illusion. _Le changement, c'est maintenant !_ Rappelle-toi, le
PS avait tous les pouvoirs en 2012 : la présidence, l'Assemblée, les
villes, les régions… merde, même le Sénat était passé à gauche ! Une
première !  Les types avaient les mains libres et carte blanche pour
tout. Il fallait écouter Copé, la pleureuse « profondément choquée »,
nous expliquer l'énorme danger que représentaient ces pleins
pouvoirs. Lutter contre la finance ? Imposer les revenus du capital
comme ceux du travail ? Interdire le cumul des mandats ?

__LOL NOPE.__

Au lieu de ça, nous aurons eu la même merde qu'avant. Parfois en pire.
Course à la croissance alors même que nous produisons déjà trop pour la
planète. Course au plein emploi alors que le travail est condamné à
disparaître (ce qui, je le rappelle, devrait être une bonne nouvelle).
Course à la productivité alors que les syndromes d'épuisement
professionnel se multiplient et que le mal-être des travailleurs se
généralise. Diminution de ce qu'on nous matraque comme étant « le coût
du travail » mais qu'un employé sensé devrait comprendre comme « mon
niveau de vie ». Détricotage méthodique des services publics qui
devraient au contraire être renforcés.

Nous n'attendions rien de Hollande, il a réussi à faire pire. Des lois
liberticides au nom d'une sécurité qu'elles ne garantiront même pas. Un
État d'Urgence à durée indéterminée. Des militants assignés à résidence
pour leurs convictions. Des manifestations politiques interdites. Des
gamins mis en garde à vue parce qu'ils ne respectent pas une minute de
silence. Heureusement que c'est sous un parti qui se dit « républicain »
que tout cela se passe, sinon, on pourrait doucement commencer à
s'inquiéter.

Vous me traitez d'irresponsable parce que je ne suis pas allé voter
dimanche ? Moi je me trouve irresponsable d'avoir légitimé notre
gouvernement actuel en votant en 2012. Depuis 2012, j'ai fait comme
beaucoup de monde : j'ai traversé le stade 4, celui de la dépression. À
me dire que nous étions définitivement foutus, que même lorsqu'un parti
qui se disait en opposition totale avec le précédent se vautrait à ce
point dans la même politique insupportable, il n'y avait plus de
solution. Que la démocratie était morte, et que nous allions crever avec
elle. Ami votant, admets-le, tu as eu la même réaction. Mais comme
toujours, à chaque vote, tu régresses, tu retournes à l'étape 3, au
marchandage, à te dire que peut-être, on pourra incliner un peu le
système en s'asseyant sur nos convictions.

Moi, j'ai passé le cap. Je suis à l'étape 5, à l'acceptation. La
démocratie représentative est morte, point. Que cela soit une bonne
chose ou non, l'avenir le dira, mais le fait demeure : ce système est
mort. Tu penses que retourner à l'étape de marchandage, c'est garder de
l'espoir et qu'accepter la mort de notre système, c'est le désespoir. Je
ne suis pas d'accord. Faire son deuil, c'est bien. C'est même nécessaire
pour passer à autre chose et, enfin, avancer.

### La démocratie est morte, vive la démocratie !

Tu remarqueras que je persiste à ajouter « représentative » quand je
parle de mort de la démocratie. Parce que je ne crois pas que la
démocratie elle-même soit morte : je pense que la démocratie _réelle_
n'a jamais vécu en France. Le système dans lequel nous vivons se
rapproche plus d'une « aristocratie élective » : nous sélectionnons nos
dirigeants dans un panel d'élites autoproclamées qui ne change jamais,
là où la démocratie voudrait que les citoyens soient tour à tour
dirigeants et dirigés. Le simple fait que l'on parle de « classe
politique » est le déni même de la notion de représentation qui est
censée faire fonctionner notre démocratie représentative : la logique
voudrait que ces politiciens soient issus des mêmes classes que celles
qu'ils dirigent. Attention, ne crachons pas dans la soupe, notre système
est bien mieux qu'une dictature, à n'en pas douter. Mais ça n'est pas
une démocratie. Je te renvoie à ce sujet au documentaire _J'ai pas voté_
(en libre accès et facilement trouvable sur Internet) que tout le monde
devrait voir avant de sauter à la gorge des abstentionnistes.

Des gens sont morts pour qu'on puisse voter ? Non, ils sont morts parce
qu'ils voulaient donner au peuple le droit à s'autodéterminer, parce
qu'ils voulaient la démocratie. Est-ce qu'on pense sérieusement, en
voyant la grande foire à neuneu que sont les campagnes électorales, que
c'est pour cela que des gens sont morts ? Pour que des guignols cravatés
paradent pendant des semaines pour que nous allions tous, la mort dans
l'âme, désigner celui dont on espère qu'il nous entubera le moins ? Je
trouve ce système bien plus insultant pour la mémoire des combattants de
la démocratie que l'abstention.

Alors oui, j'ai fait mon deuil, et ça me permet d'avoir de l'espoir pour
la suite. Parce que pendant que la grande imposture politicarde se
poursuit sur les plateaux-télé, nous, citoyens de tous bords, essayons
de trouver des solutions. Plus le temps passe, plus le nombre de gens
ayant terminé leur deuil augmente, plus ces gens s'intéressent
réellement à la politique et découvrent des idées nouvelles, politiques
et sociétales : tirage au sort, mandats uniques et non-renouvelables,
revenu de base, etc. Des solutions envisageables, des morceaux de
savoir, de culture politique… de l'éducation populaire, en somme. Rien
ne dit que ces solutions fonctionneront, mais tout nous dit que le
système actuel ne fonctionne pas. Et lorsque ce système s'effondrera, ce
sera à ces petits morceaux de savoir disséminés un peu partout dans la
population qu'il faudra se raccrocher. L'urgence aujourd'hui, c'est de
répandre ces idées pour préparer la suite. Ami votant, tu as tout à
gagner à nous rejoindre, parce que tu as de toute évidence une
conscience politique et qu'elle est gâchée, utilisée pour te battre
contre des moulins à vent.

Notre système est un vieil ordinateur à moitié déglingué. Tu peux
continuer d'imaginer qu'en réinstallant le même logiciel (PS ou LR,
choisis ton camp camarade), il finira par fonctionner. D'autres
utilisent la bonne vieille méthode de la claque sur la bécane (le vote
FN) : on sait bien que ça ne sert à rien et que ça ne va certainement
pas améliorer l'état de l'ordi, mais ça soulage. Certains imaginent
qu'en déboulonnant l'Unité Centrale et en hackant petit à petit le
système, on finira par faire bouger les choses (la députée Isabelle
Attard est un bon exemple, personnellement je la surnomme l'_outlier_,
la donnée qui ne rentre pas dans le modèle statistique du politicien).
Ce n'est pas la pire des idées. On a même parlé de rebooter la France.
Qui sait, si on arrive à mettre sur pied une telle stratégie en 2017,
possible que je ressorte ma carte d'électeur du placard. Mais les plus
nombreux, les abstentionnistes, ont laissé tomber le vieil ordinateur
et cherchent juste à en trouver un nouveau qui fonctionne.

Alors on fait quoi ? Pour être clair, je suis comme tout le monde, je
n'ai aucune idée de la manière dont on peut passer à autre chose, pour
instaurer une vraie démocratie. Une transition démocratique pourrait
s'opérer en douceur en modifiant les institutions petit à petit : tout
le monde aurait à y gagner. Politiciens compris, car l'alternative est
peut-être l'explosion, et c'est une alternative à l'issue très
incertaine. Mais clairement, nous ne prenons pas la direction d'une
transition non-violente.

Je continue pour ma part à penser que, comme le disait Asimov, « la
violence est le dernier refuge de l'incompétence ». Mais nous constatons
chaque jour un peu plus notre impuissance dans ce système, et les
politiciens actuels seraient bien avisés de corriger le tir avant qu'il
ne soit trop tard. Avant que les citoyens ne se ruent dans ce dernier
refuge.

[^deuil1]: Je précise : 6 millions de votes FN pour 66 millions de
    français.  Oui, ça compte les non-inscrits et les mineurs, mais
    l'image que j'avais, c'est que si je me retrouve demain dans une
    foule lambda en France et que je regarde autour de moi, moins
    d'une personne sur 10 aura été un électeur du FN. On est loin de
    la vague bleue marine annoncée…

[^deuil2]: Mon « ami votant » n'est pas à prendre sur un ton condescendant :
    je suis _réellement_ amical ici, parce que je pense que nous sommes
    dans le même bateau. Désolé si le ton peut paraître hautain, ce
    n'est pas l'intention :)


## Votants, vous n'avez pas honte ? [lf_025]

## En marche (ou crève) [enmarche]

_Article publié le 27 avril 2016_

On a parfois un peu l'impression de se répéter quand on parle de
déconnexion entre la classe politique et le reste de la population. Mais
il faut avouer que nos non-représentants s'appliquent si régulièrement à
enfoncer le clou qu'on n'en voit plus la tête. Par exemple, prenez
Emmanuel Macron, banquier d'affaire et membre d'un gouvernement
estampillé « socialiste », un mot qui ferait hurler n'importe quel
banquier dans un paysage politique où les mots auraient encore un sens.

Bah vous voyez, quand ce type là nous sort _En Marche !_ (avec un point
d'exclamation, oui, comme _Yahoo!_) mouvement ni de gauche ni de gauche
qui nous promet de réinventer la politique en faisant exactement la même
chose qu'avant, ça me fascine. Et le cortège de médias qui en fait ses
gros titres alors que tout le monde s'en bat les reins... oui, pardon
aux familles, tout ça, mais Macron et ses manachronismes, TOUT LE MONDE
S'EN TAPE. Mais les médias ont décidé que Macron, c'était maintenant
l'homme de la gauche, l'homme avec le vent en poupe que c'est pour lui
qu'tu dois voter si qu't'es à gauche (d'ailleurs ils ont choisi Alain «
Emplois Fictifs » Juppé pour la droite, si vous n'aviez pas suivi).
Macron. Le type avec un sourire Colgate qui nous balance une vidéo façon
publicité pour serviette hygiénique avec voix off d'hôtesse de l'air :

> « Quand on écoute les Français, on entend partout la même chose. Il
> faudrait que ça bouge. Il faudrait essayer des idées neuves, aller
> plus loin, oser, en finir avec l'immobilisme. \[...\] Alors on fait
> quoi ? On se met en marche. Car on ne fera pas la France de demain
> sans faire place aux idées neuves, sans audace, sans esprit
> d'invention. On ne fera pas la France de demain, en restant isolé de
> ce nouveau monde à la fois inquiétant et plein d'opportunités. On ne
> fera pas la France de demain sans faire place à une génération
> nouvelle, combative, entreprenante, audacieuse et belle. Oui… Il est
> temps de se mettre en marche. »

Ou comment broder de la parfaite communication de marketeux décérébré
sur du vide, du bon gros vide bien enrobé d'une grosse couche de vernis
à gerber. Et pour les quelques vagues concepts qui ressortent (au-delà
du concept de « mouvement » qui est une constante de la politique
– d'ailleurs le changement, n'était-ce pas hier ?), rien de nouveau sous
le soleil : flexibilité, réformes NÉ-CE-SSAIRES et mondialisation
heureuse. Nous sommes sauvés. Le système représentatif va fonctionner et
l'abstention va baisser grâce à l'énorme reprise de confiance envers la
classe politique qu'un mouvement comme _En Marche !_ ne va pas manquer
de générer.

Oui, la relève est assurée. De nouveaux guignols en costumes, bien
peignés, plus lisses qu'une plaque de verglas, qui font des mouvements,
des contre-mouvements, des _think tanks_ et autres concepts foireux pour
ne pas dire qu'on se paluche joyeusement le poireau en réfléchissant à
la couleur des chaînes. Qui se matent le nombril en comité fermé avec
leurs potes journalistes à la télé en étant persuadés de représenter
« les Français ». Ça me rappelle une chanson de Pink Floyd, _The
Fletcher Memorial Home_. Si vous ne la connaissez pas (et ce serait
compréhensible, elle vient du peu connu _The Final Cut_ qui est presque
un album solo de Roger Waters en réalité), je vous livre une traduction
personnelle du premier couplet :

> Éloignez tous vos enfants attardés
>
> Et construisez leur une maison
>
> Un petit endroit rien que pour eux
>
> Le Mémorial Fletcher pour tyrans et rois incurables
>
> Et ils pourront s'y voir tous les jours
>
> Sur un réseau télé en circuit fermé
>
> Pour s'assurer qu'ils existent toujours
>
> C'est bien la seule connexion qu'ils puissent ressentir

À chaque fois que j'entends ce couplet, j'ai les portraits de nos
politiciens et de ~~nos~~ leurs journalistes en tête. Et je me demande
si, un jour, on ne pourrait pas faire ça. Puisqu'il semble difficile de
les déloger du pouvoir, les laisser entre eux, les laisser jouer. « Oh,
tiens, j'vais faire un mouvement. » « Pour faire joli sur ton CV, tu
préfères un poste de Haut Commissaire de mon Cul ou de Conseiller de mes
Couilles ? » « Oh, c'est moi que j'ai le plus gros parti. » « Bisque
bisque rage. »

Il est pour ma part de plus en plus clair que nous n'avons pas besoin
d'eux[^enmarche1] (ou, dans une moindre mesure, que nous ne serions en
tout cas pas moins bien lotis sans eux). Mais là, je me rends compte
qu'ils n'ont peut-être pas besoin de nous non plus. Ils ne se
rendraient même pas compte si nous n'étions plus là. Macron continuera
à se passer la brosse à reluire qu'on soit derrière lui ou
pas. D'ailleurs, pour ce que ça vaut, on n'y est pas, derrière lui, et
il n'a même pas fait gaffe.

Oui, peut-être qu'il faudrait acter la séparation du peuple et des
pseudo-élites. Et essayer autre chose de notre côté. Sans eux. C'est un
peu ce que _Nuit Debout_ essaie de faire, j'imagine. Bien emmerdant pour
les politiciens qui auront du mal à récupérer ce mouvement puisqu'il
s'est précisément construit contre eux. Alors il vaudra mieux pour eux
s'appliquer à le salir, le détruire. Ça a commencé.

À ce titre, quand j'entends Jean-François « Profondément choqué » Copé
dire de la _Nuit Debout_ « ils sont tellement coupés de la réalité », ça
me fait pisser de rire. Le mec qui passe ses vacances dans des villas
luxueuses de marchands d'armes, touche un salaire mensuel à 5 chiffres
(et estime que seuls les minables acceptent des boulots à moins de 5000
euros par mois) sans parler des privilèges octroyés par ses nombreux
mandats à nos frais va t'expliquer que t'es coupé de la réalité, jeune
con révolutionnaire. Et je veux bien entendre toutes les critiques du
monde à l'encontre de la _Nuit Debout_, hein. Mais simplement, pas de la
part d'un professionnel de la politique. Pas de la part de Copé.
Sérieusement, c'est comme si Nabilla reprochait à Frédéric Lordon de
manquer de culture économique.

Et puis à côté de ça, les journaux qui titrent, scandalisés, « la _Nuit
Debout_ révèle son vrai visage ! » suite à l'expulsion d'Alain
« Taisez-vous » Finkielkraut de la Place de la République. Son vrai
visage ? Parce qu'il vous a fallu ce non-événement pour comprendre que
_Nuit Debout_ se positionnait (entre autres) contre tous les défenseurs
de l'ordre établi qui monopolisent les plateaux télé pour dicter
unilatéralement ce qui est bon pour nous depuis 30 ans ?

« S'attendaient-ils vraiment à ce que nous les traitions avec le moindre
respect ? » s'interroge Waters dans la suite de la chanson. Je ne
défends pas spécialement la méthode qui consiste à hurler sur quelqu'un
jusqu'à ce qu'il s'en aille. Mais pitié, ne faites pas comme si ça
sortait de nulle part, comme si c'était gratuit. Lordon l'explique bien
mieux que je ne saurais le faire :

> « Nous ne sommes pas ici pour faire de l'animation citoyenne “all
> inclusive” comme le voudraient Laurent Joffrin et Najat
> Vallaud-Belkacem. Nous sommes ici pour faire de la politique. Nous ne
> sommes pas amis avec tout le monde. Et nous n'apportons pas la paix.
> Nous n'avons aucun projet d'unanimité démocratique. Nous avons même
> celui de contrarier sérieusement une ou deux personnes. »

Pauvre Finkielkraut qu'on ne veut même pas entendre, quelle atteinte à
la démocratie. C'est vrai qu'on a tellement peu l'habitude de
l'entendre, sa douce voix. Si on était taquins, on remarquerait que pour
que toutes les personnages présentes Place de la République à Paris
rattrapent leur écart de temps de parole publique avec Finkielkraut,
celui-ci devrait probablement la boucler pendant les 2 prochains siècles
(ça nous ferait des vacances, notez).

Fort heureusement, tous les grand médias pourront à l'unisson s'en
offusquer et corriger cette honteuse injustice. Jusqu'à l'apothéose avec
Michel « En roue libre » Onfray qui nous sort l'accusation
d'antisémitisme et de nazisme du chapeau (car il n'y a tellement rien à
reprocher à Finkielkraut dans ses paroles et ses actes que toute action
à son encontre ne peut qu'être motivée par un racisme latent). Mais
quand Finkielkraut loupera une marche, il se trouvera bien un Onfray
pour accuser l'escalier d'antisémitisme. Pendant que le reste de la
population (_Nuit Debout_ incluse), comme d'habitude, ignorera un énième
non-événement monté en épingle et passera à autre chose.

Parce que les Macron, les Finkielkraut, les Copé, les Onfray et tous les
autres, ce sont des bourdonnements dans nos oreilles, une nuisance
permanente avec laquelle nous avons appris à vivre faute de mieux. On
peut cesser d'y prêter attention, mais on ne peut pas cesser de les
subir car ce sont toujours eux qui tiennent les rênes, sans relâche. Et
au bout du compte, c'est bien à cela que _Nuit Debout_ (et d'autres)
cherchent tant bien que mal une solution. Faire en sorte que ces élites
auto-proclamées continuent de jouer dans leur coin si cela les amuse,
mais qu'elles cessent de nous nuire.

Alors je ne sais pas ce qu'il adviendra de _Nuit Debout_. Peut-être que
ça finira en eau de boudin. Que tout le monde rentrera chez soi et que
le monde continuera de (mal) tourner. Mais il en restera de toute
manière une expérience mille fois plus enrichissante et porteuse
d'espoir que tous les _En Marche !_ de tous les Macron du monde. « On ne
fera pas la France de demain sans faire place à une génération nouvelle,
combative, entreprenante, audacieuse et belle » nous disait l'hôtesse de
l'air dans son insipide vidéo.

Rassurez-vous, une génération combative arrive. L'ennui, c'est que
l'ennemi à combattre, c'est vous.

[^enmarche1]: Lire à ce sujet _Le deuil de la démocratie
    représentative_ en ligne et dans le tome I de Grise Bouille.
    
## Chers amis étrangers|, voilà pourquoi certains d'entre nous ne sont pas ravis par l'élection de Macron [macron]

_Article publié le 10 mai 2017_

Dimanche soir, lorsqu’il a été annoncé qu’Emmanuel Macron était élu
Président de la République, il y a eu un gros écart dans les réactions
que l’on a pu observer sur les réseaux sociaux : de nombreux étrangers
nous ont félicités (même des icônes de la pop culture comme Mark
Hamill ou Zach Braff) mais parmi les français, on a aussi vu pas mal
de réactions résignées et blasées. Et c’est assez étrange d’être
félicité pour quelque chose dont on n’est pas fiers. Je vais essayer
d’expliquer pourquoi.

### Ouais ! On a battu Le Pen ! Vraiment ?

Tout d’abord, évacuons ça : OUI, nous sommes heureux que Marine Le Pen
ne soit pas présidente, nous n’aurons pas à subir sa politique basée sur
le rejet et la haine de l’étranger et sur un nationalisme décomplexé.
Elle aurait sans doute été dangereuse pour beaucoup de gens dans le pays
et son élection aurait eu des conséquences terribles en général (c’est
un peu notre Donald Trump personnel, et ça en dit long).

Le seul problème, c’est que, bien qu’elle n’ait pas gagné, il y a de
grandes chances que rien ne soit résolu avec Macron : les 11 millions de
personnes qui ont voté pour Le Pen ne vont pas disparaître comme par
magie après l’élection. Pire : la plupart des raisons qui expliquent le
score élevé de Le Pen sont précisément le résultat des politiques que
Macron défend.

Tout d’abord, il faut comprendre que les gens qui votent Le Pen ne
sont pas tous des racistes ou des nationalistes[^macron1] (heureusement). La
plupart sont juste des gens qui voient leurs niveaux de vie fragilisés
par la mondialisation. Et quand Le Pen parle de les protéger contre ça
(en allant jusqu’à piquer des principes économiques théorisés à
l’origine par la gauche), ils l’entendent. Michael Moore avait dit la
même chose à propos de l’élection de Trump[^macron2], et je pense qu’il a
raison.

On peut facilement voir, par exemple, que les villes ont largement voté
Macron (90% à Paris) alors que dans les campagnes, Le Pen a dominé et a
parfois obtenu des scores énormes (jusqu’à 100% dans certains villages).
Même dans des coins où l’immigration est basse voire inexistante. Et
c’est facile, pour les citadins, de penser « oh, ces abrutis de pécores
racistes » et de cacher le problème sous le tapis, tout comme c’est
facile pour les gagnants de la mondialisation de l’encourager avec
enthousiasme. Oh, et je suis un jeune ingénieur en informatique, alors
vous en faites pas pour moi : je fais partie des gagnants. Mais
contrairement à Macron et certaines de mes connaissances, je ne pense
pas que la France soit uniquement constituée de jeunes cadres
dynamiques.

Les habitants de la campagne savent que dans leurs villages, avant il
y avait des écoles ; avant, il y avait des bureaux de poste ; avant,
il y avait des médecins ; avant, il y avait des petits commerces ;
avant, nous avions des entreprises et services publics puissants et
efficaces. Oh, et le budget de l’état était à l’équilibre. Il y a une
idéalisation, certes, mais pas tant que ça.

Maintenant, leurs gamins doivent faire un long trajet en bus pour
atteindre l’école la plus proche et ses classes surchargées ; ils
doivent prendre leur voiture pour aller poster une simple lettre ; ils
prient pour ne pas faire d’infarctus puisque les hôpitaux sont fermés et
fusionnés les uns après les autres, remplis d’infirmiers et infirmières
en burn-out ; ils se rendent compte que ce qui a été privatisé leur
coûte plus cher et ne fonctionne pas mieux quand ce n’est pas pire
(autoroutes, électricité, chemins de fer…).

Et concernant les commerces, eh bien, ils peuvent se consoler avec les
immondes centres commerciaux géants qui poussent comme des champignons
partout dans les périphéries et qui transforment les champs en
parkings géants[^macron3]. Ces temples de la consommation inhumains qui
tuent les petits commerces autour et effacent les spécificités
culturelles des régions de France. Alors oui, quand Le Pen parle de
tradition et d’héritage culturel français, tu parles qu’ils écoutent.

Les gens ne sont pas anti-Europe par essence ou par dogme : ils le sont
parce qu’ils se rendent compte que l’UE, ces derniers temps, a surtout
œuvré pour libérer le marché, pas les peuples ; qu’elle a surtout
consisté à mettre les européens en compétition les uns contre les
autres, en nivelant vers le bas la qualité de vie avec celles des pays
les plus pauvres au lieu de niveler vers le haut la protection sociale
avec celles des pays les plus riches. Si vous demandez aux gens, ils
sont tous en faveur de l’union des peuples et de la paix éternelle entre
les pays, mais ils ne sont pas idiots : ils se rendent bien compte que
l’élite de l’UE manipule ces aspirations comme un prétexte pour forcer
leur projet de capitalisme libéral de prédation.

Je suis pour l’Europe, mais soyons honnêtes : l’Europe promue par Macron
et par tant d’autres politiciens pro-Europe est la meilleure arme contre
l’Europe elle-même. On ne peut pas attendre des gens qu’ils soutiennent
une politique qui se traduit pour eux en austérité et précarité. On peut
se féliciter que la France n’ait pas suivi le Royaume Uni après le
Brexit, mais l’Europe n’est pas soudainement redevenue populaire avec
l’élection de Macron. Vous voulez que les gens soutiennent l’Europe ?
Construisez une Europe qui soutienne les gens.

### Non, Macron n’est pas un renouveau

Qu’est-ce que Macron dans tout ça ? Macron est le pur produit du système
qui a créé ces problèmes. J’ai lu dans beaucoup d’articles étrangers que
Macron était « un renouveau », « un vent de changement » dans la
politique française. À ce niveau, ce ne sont plus des *fake news* : ce
sont de grosses conneries.

Laissez-moi vous résumer ça : Macron a été formé à l’ENA, une école
fréquentée par à peu près 90% de nos politiciens (ils sont tellement
des copies les uns des autres qu’on a inventé une expression pour :
« énarques ») ; il était ministre sous le dernier président François
Hollande et a été l’inspirateur de sa politique économique ; il a
bossé comme banquier chez Rothschild, ce qui fait qu’il n’était pas
hors du système (capitaliste) mais plutôt au sommet de ce système ; et
la cerise sur le gâteau, il est soutenu par la moitié des dinosaures
de la politique qui avaient déjà le pouvoir avant qu’il ne soit au
lycée.

Il est jeune ? Ça nous fait une belle jambe. Le projet de Macron est à
peu de chose près dans la continuité de ce qui a été fait dans notre
pays depuis trente ans : réduction des droits des travailleurs,
réduction de la protection sociale, accroissement de la pression de la
compétition sur les gens en signant des accords de libre-échange avec
des pays dont le code du travail est une blague. Comme s’il y avait quoi
que ce soit à gagner à tenter d’être « compétitif » avec des
travailleurs chinois ou bangladais. Le projet de Macron est exactement
ce qui a poussé 11 millions de personnes vers Marine Le Pen.

C’est d’ailleurs l’une des raisons qui ont fait que beaucoup de gens de
gauche (moi compris) étaient réticents à voter Macron pour « faire
barrage » à Le Pen : en quoi voter pour le kérosène va « faire barrage »
au feu ?

### Mais la victoire Macron est incontestable, n’est-ce pas ?

C’est une autre raison qui a fait que des gens étaient réticents à voter
Macron et même en colère contre l’idée : la façon dont l’élection a été
arrangée. Pendant une grosse année, presque tous les grands médias ont
fait massivement campagne pour Macron.

Comme une prophétie auto-réalisatrice, un an saturé de sondages et
d’articles pro-Macron ont fait que beaucoup de gens ont voté pour lui,
non par conviction mais parce qu’ils ou elles pensaient qu’il était le
« plus approprié pour gagner », quel que soit le sens qu’on donne à
cette idée. Un sondage a montré que seulement 60% des gens qui ont
voté Macron au premier tour l’ont fait par conviction[^macron4], un score
qui s’élève à 80% chez n’importe quel⋅le autre candidat⋅e.

En parallèle, le Front National (parti de Marine Le Pen) s’est aussi
vu accorder une large couverture médiatique, le présentant comme le
choix contestataire par défaut. Le but, à peine caché[^macron5], était
d’avoir ce second tour Macron-Le Pen. Ce second tour est très
pratique, parce que si Le Pen s’oppose bien au libéralisme brut de
Macron (et encore, si on veut : le libéralisme ne la dérange pas tant
qu’il est circonscrit au territoire de la France), elle construit ça
sur de l’hyper-conservatisme, de la xénophobie et de
l’europhobie. Donc bien entendu, beaucoup de gens (comme moi) n’iront
jamais jusqu’à simplement envisager de voter pour elle parce qu’ils ou
elles souhaitent une opposition juste, démocratique et respectable au
libéralisme.

Et bien sûr, les médias qui avaient donné la parole au FN pendant des
mois furent les mêmes qui exigèrent avec autorité que les électeurs
éteignent ce feu qu’ils avaient allumé.

Depuis de nombreuses années déjà, Le Pen et le Front National ont
servi, pour les médias et les autres partis, d’arme contre la
démocratie : mettez n’importe qui en face du FN dans une élection et
il ou elle est presque certain de l’emporter. Et puisqu’ils ont
compris qu’ils peuvent façonner ce choix impossible, les grands médias
ont juste à présenter un unique candidat comme le choix raisonnable
(de préférence, celui qui sert les intérêts des propriétaires de ces
grands médias), à laisser le FN monopoliser le reste de la parole et
le tour est joué.

Il suffit de voir ce qui s’est passé quand un autre candidat commençait
à avoir de plus en plus d’avis favorables : Jean-Luc Mélenchon, qui est
également anti-libéralisme mais avec des principes basés sur l’écologie
et la refonte de notre démocratie à la place de la xénophobie et de
l’hyper-conservatisme (avec l’ambition de convoquer une assemblée
constituante pour réformer notre république qui fonctionne mal). Pour
information, ce mec était soutenu par une grande majorité d’ONG et par
des gens comme Noam Chomsky. Mais dès qu’il y a eu une chance de le voir
accéder au second tour, tous les grands médias ont lâché les chiens sur
lui, l’accusant d’être un stalinien ou de vouloir créer un second
Venezuela en France.

Même François Hollande, oui, le Président de la République qui
s’autoproclame socialiste (la blague), est sorti de sa réserve et s’est
élevé contre Mélenchon, un candidat de gauche, et n’a jamais levé le
petit doigt contre l’extrême-droite de Le Pen qu’on annonçait pourtant
comme vainqueur du premier tour depuis des plombes. Pourquoi ? Parce que
contrairement à Le Pen, Mélenchon était une vraie menace pour Macron.

Ne vous méprenez pas, Mélenchon n’était pas le messie et il y avait bien
sûr des points discutables dans son programme. Mais il était la voix
d’une critique rationnelle et positive du libéralisme, une qui aurait pu
se confronter à la vision de Macron. En tout cas, si Mélenchon avait été
au second tour, il aurait peut-être perdu aussi, mais peut-être pas : et
croyez-moi, le débat entre les deux tours aurait eu une autre gueule que
le crêpage de chignons imbécile auquel on a eu droit avec la guignolo de
Le Pen contre Macron.

### Combien de temps cela va-t-il tenir ?

Le père de Le Pen était au second tour de l’élection présidentielle il y
a quinze ans et nous avons joué au même jeu : les gens ont voté pour
Chirac pour faire barrage à Le Pen même si pas mal d’entre eux n’avaient
aucune sympathie pour Chirac. Chirac a obtenu plus de 80% des votes. On
aurait pu penser qu’il allait appliquer une politique pour réparer cette
société française et écouter la contestation populaire ? Loupé. Il a
fait sa politique de droite comme si 80% des gens avaient voté pour lui
par conviction. Et il y a de grandes chances que Macron fasse la même
chose.

Le truc, c’est que cette stratégie devient dangereuse : les électeurs
comprennent très bien ce qui se passe et ils sont de moins en moins
enclins à participer à cette mascarade[^macron6].  Chirac a fait 80% en
2002, mais Macron n’a eu « que » 66% des votes contre Le Pen au second
tour, accompagné d’un record d’abstention et de votes blancs. Et alors
que Le Pen a entraîné presque deux fois plus d’électeurs que son père
quinze ans plus tôt, Macron a vu moins de gens d’accord pour voter
pour lui contre leurs propres convictions.

Malheureusement, il y a de grandes chances que cela ne s’arrange pas
dans un futur proche : personne dans les hautes sphères ne semble
vouloir mettre fin à ce jeu dangereux et dans cinq ans, Le Pen pourrait
bien finir par gagner pour de bon (si les choses ne se gâtent pas plus
tôt).

Dans le même temps, Macron va appliquer sa politique libérale comme si
tout allait bien. Lui et les médias vont continuer de marteler « IL N’Y
A PAS D’ALTERNATIVE » en réponse à toute critique contre ça. De plus en
plus de gens n’auront plus rien à perdre à s’abandonner à n’importe quel
vote pour dire « assez ! ». Je ne vois pas comment cela pourrait finir
bien.

Alors dans les prochains mois, vous entendrez peut-être qu’il y a encore
une fois des grèves en France. Et avant que vous ne commenciez à blaguer
sur le fait que nous sommes constamment en grève, dites-vous bien que
nous essayons de protéger notre modèle social contre une énième attaque,
et que nous sommes seuls, avec pratiquement tous les pouvoirs
médiatiques et politiques français contre nous.

Notre modèle social a été notre force pendant toute la seconde moitié
du XXe siècle, ce qui a par exemple mené à la création d’un système de
santé reconnu mondialement. Et ce modèle social a été mis en place
juste après la Seconde Guerre Mondiale, à un moment où le pays était
dévasté et ruiné (ça, c’est pour le « on ne peut pas se le
permettre »). Et il n’est ni dépassé ni inadapté : l’une des raisons
qui fait que les français ont moins souffert de la crise des subprimes
en 2008 que d’autres, ça a été précisément la robustesse de ce modèle
social[^macron7].  Et je sais que nous avons la réputation de capituler
aisément, mais sachez une chose : nous ne le laisserons pas crever
sans combattre.

[^macron1]: _« Mon voisin vote Front national »_, par Willy Pelletier,
    publié en janvier 2017 dans _Le Monde diplomatique_.

[^macron2]: _5 Reasons Why Trump Will Win_, par Michael Moore, publié sur
    son blog en juillet 2016.
    
[^macron3]: _Valbonne: plus de 7000 signatures contre Open Sky_, un énième
    projet de centre commercial géant nécessitant de raser une partie
    de la forêt de la Valmasque, par V.B., publié le 14 mars 2017 sur
    le site de _Nice Matin_.

[^macron4]: _La France de Macron, un vote par défaut_, par Grégoire Biseau,
    Luc Peillon et BIG, publié le 25 avril 2017 sur le site de
    _Libération_.

[^macron5]: _Emmanuel Macron has taken French voters for granted. Now he
    risks defeat_, par Olivier Tonneau, publié le 1er mai 2017 sur le
    site de _The Guardian_.

[^macron6]: _Vote utile, citoyenneté et radicalité_, par Bobille, publié le
    6 mars 2017 sur _Le Club de Mediapart_.

[^macron7]: _Bilan financier mondial et leçons de la crise_, par Marie-Anne
    Kraft, publié le 28 mars 2009 sur _Le Club de Mediapart_.

## Lettre(s) aux français [lf_032]

# Répartition des richesses

## Le cadre [lf_022]

## Oui, le travail disparaît [lf_023]

## Une histoire de poissons [lf_003]

# Pensée dominante

## Ailleurs, c'est pire [lf_009]

## Réglons le problème du chômage [lf_027]

## Baisse de « charges » [lf_031]

# Effondrement & renaissance

## Collapsologie & psychohistoire [lf_030]

## Quel est votre rêve ? [reve]

_Article publié le 4 mai 2018_

### Derrière la rage, la question

On ne va pas se mentir, je ne suis pas le dernier pour gueuler : des
articles pour conchier tel ou tel gouvernement, j’en ai fait deux
trois ; des manifs aussi ; quand d’autres gueulent – cheminots,
personnel hospitalier, étudiants… –, je ne manque pas de les soutenir.

Derrière les revendications, derrière la rage à chaque attaque envers
les acquis sociaux, me vient souvent une question, lancinante,
angoissante même : _à quoi bon_ ?

Pourquoi tout ce cirque ? Vers quoi se dirige-t-on ? Où veut-on aller ?
On nous dit que monde a changé, qu’il faut s’adapter. Comme si, au
passage, ce « changement » était l’opération du Saint Esprit ; comme si
c’était le travailleur bangladais qui nous avait supplié de venir
l’exploiter pour que dalle ; comme si ça n’était pas _nos propres
dirigeants_ qui avaient vendu le monde entier au libéralisme le plus
sauvage, ceux-là même qui nous disent que « le monde a changé ».

Et après ? Quand on se sera adapté à ce « monde qui a changé » (et que
quand même, c’est pas de chance), il se passera quoi ? C’est quoi,
« demain » ? Les lendemain qui chantent ? Quand on aura dit adieu à
toute protection sociale, quand on aura libéralisé tout ce qui ne
l’est pas encore, quand on aura mis un prix et un marché sur le
moindre éléments de vie humaine, il se passera quoi ? La terre
promise ? Le paradis sur Terre ? C’est quoi, le bout du chemin ?

Cette question qui devrait être notre moteur, ce qui devrait nous faire
nous lever le matin : _quel est notre rêve ?_

Alors je sais, c’est pas simple, comme question – et j’vous parle même
pas de la réponse. Seulement, il faudrait déjà se la poser, la
question.  Quand je vois les politiques de nos politiciens, les éditos
de nos éditocrates, les expertises de nos experts, les arnaques de nos
énarques, je brûle de leur poser la question : quelle société
souhaitez-vous construire avec votre idéologie capitaliste dont vous
nous soutenez qu’elle est la seule voie possible ? Où est-ce qu’elle
nous mène, Tina[^reve1] ? Quel est votre monde idéal ?

_Quel est votre rêve ?_

### Mon rêve

Le truc, c’est que moi, si j’y réfléchis, je peux assez facilement
l’envisager, mon rêve, ma vision de la société idéale dans laquelle
j’aspire à vivre.

Je sais déjà qu’elle serait soutenable à long terme, tant écologiquement
qu’humainement. On y aurait considérablement réduit l’activité humaine
en stoppant la course à la croissance et à la surconsommation. Avec des
besoins énergétiques drastiquement revus à la baisse, on serait en
mesure de les assurer par des énergies et matières premières
renouvelables. Ce qui aurait par la même occasion réduit la pression
autour des énergies et matières premières au Moyen-Orient (pétrole, gaz,
etc.) ou en Afrique (métaux rares, uranium, etc.), asséchant la source
de nombreuses guerres, rendant le monde plus stable[^reve2].

Activité humaine réduite, cela voudrait aussi dire que l’on
travaillerait beaucoup moins : uniquement le matin par exemple. Le
reste de la journée serait dédié à la gestion politique de la société
(on va y revenir), à la vie sociale (famille, amis), aux activités
bénévoles et bien sûr aux loisirs. Le travail serait recentré autour
des besoins des êtres humains, on aurait supprimé cette stupidité de
« créer des besoins » dans un monde incapable de subvenir à ceux déjà
existants – car inhérents à la condition humaine – de tous. On aurait
aussi logiquement interdit la publicité[^reve3], premier pollueur
d’esprit voué à détruire l’humain sur l’autel de la marchandisation
généralisée[^reve4].

Les travaux les plus difficiles et fatigants qui n’auraient pas pu être
automatisés seraient les mieux payés et les plus largement répartis. On
travaillerait de moins en moins à mesure que l’on vieillirait jusqu’à
arriver à la retraite totale (comme actuellement) à un âge où nous
serions encore suffisamment en bonne santé pour en profiter[^reve5].

Parlons rémunération, tiens : l’argent ne serait plus généré par le
crédit bancaire mais par chaque personne de manière régulière de par sa
propre existence[^reve6] (sur le modèle d’un revenu universel de création
`LATEX` monétaire, comme la monnaie numérique \u{G}[^reve7]). Ce revenu serait à la fois
`EPUB` monétaire, comme la monnaie numérique Ğ1[^reve7]). Ce revenu serait à la fois
une sécurité économique et une force politique, puisque ce serait chaque
citoyenne et chaque citoyen (et non les banques) qui déciderait de
comment investir sa force de travail et de création : charge à celles et
ceux qui auraient des projets ambitieux de rassembler leurs revenus à
plusieurs pour financer telle ou telle entreprise commune.

Au niveau de l’organisation de la société, on aurait mis fin à
l’aristocratie électorale[^reve8] en mettant en place une démocratie
populaire à tous les étages de la société sur le modèle de
l’autogestion : des assemblées populaires de tailles diverses seraient
organisées pour gérer telle commune par les habitants de cette
commune, telle région par les habitants de cette région, et bien sûr
tel pays par ses propres habitants (et ensuite, le monde ?). Ces
assemblées pourraient être tirées au sort[^reve9] ou, à la limite, par
un système électoral _très strictement_ encadré : mandat unique avant
inéligibilité à vie (pour éviter l’apparition d’une caste de
professionnels de la politique), obligation de représentativité des
assemblées (de classe, d’âge, de genre, d’origine),
dé-personnalisation maximale des propositions politiques, etc.

Comme il n’aura échappé à personne qu’aujourd’hui, pas mal de très
grosses entreprises sont plus puissantes que les États, il me
semblerait également logique d’étendre la sphère de la gestion
populaire collective au secteur des entreprises : il n’y a aucune
raison que la démocratie s’arrête aux portes des
entreprises[^reve10]. Des grands groupes comme Total ou Areva peuvent
largement participer à déstabiliser des régions entières du globe en
favorisant la pression sur les matières premières, sans parler de
peser un poids très lourd sur les politiques énergétiques mises en
œuvre par le pays : il me semblerait donc normal qu’elles soient
gérées à 100 % par les citoyens, pour éviter que les intérêts privés
de quelques-uns ne pèsent un poids démesuré sur le sens de
l’histoire[^reve11].

Notez que ce principe de gestion collective des moyens de productions
pourrait être assoupli selon l’échelle d’une entreprise : les TPE ou
PME n’ont qu’un pouvoir très local et limité et pourraient donc n’être
gérée que partiellement par les citoyens, voir n’être gérée que par les
gens qui y travaillent (le boulanger du coin n’a peut-être pas besoin
d’un CA populaire de 300 personnes).

Bien sûr, ce ne serait pas le paradis. On aurait fait une croix sur pas
mal de confort (surtout pour nous occidentaux), conséquence logique de
la diminution (voire suppression) des activités non-soutenables à long
terme : on aurait par exemple _beaucoup_ moins d’appareils électroniques
et on les garderait beaucoup plus longtemps (et heureusement puisqu’on
les paierait à un prix normal par rapport au travail de ceux qui les
auraient construits et à l’empreinte écologique, c’est-à-dire très
chers) ; on voyagerait sans doute beaucoup moins souvent et beaucoup
moins loin (prédominance des transports en commun et des moyens de
transport légers comme le vélo).

Et pourtant j’ai tendance à penser qu’on vivrait mieux, que le niveau de
vie ne se mesure pas simplement à la quantité de choses que l’on
consomme : vivre plus modestement, moins confortablement mais aussi plus
doucement, avoir du temps pour soi, du temps à passer avec ses proches,
du temps pour _vivre_ en somme… ça peut être un choix de société
désirable, non ?

### Certes, ce n’est qu’un rêve…

Voilà, ça, c’est mon rêve, mon petit monde idéal, là où j’aimerais qu’on
aille. Oh, je ne suis pas un grand benêt naïf, je sais bien qu’on en est
loin et que ça ne se fera pas en claquant des doigts ; que je ne verrais
sans doute pas ce monde de mon vivant ; qu’il est peut-être impossible
que nous y arrivions même un jour, même si nous le souhaitions tous
unanimement (ce qui n’est pas le cas, bien entendu).

Mais c’est vers là que je voudrais que le monde se dirige, même si c’est
à la façon d’une limite mathématique inatteignable mais dont nous
pouvons nous approcher autant que possible. C’est un peu ma boussole
politique : lorsque je vois une action politique qui nous rapproche de
ce monde idéal, même un tout petit peu, alors je considère que c’est une
bonne politique. À l’inverse, si je vois une action politique qui nous
en éloigne, même un tout petit peu, alors que je considère que c’est une
mauvaise politique.

Ça ne veut pas dire que chacun doit avoir le même monde idéal que moi.
Je ne doute pas que mon monde idéal en fasse hurler deux ou trois. Même
les gens qui ont à peu près la même sensibilité politique que moi
doivent avoir un idéal, un rêve différent. Et rien n’empêche qu’en
chemin, on se rende compte qu’en fait, on préférerait bifurquer et aller
un peu ailleurs, finalement.

Seulement, depuis pas mal de temps (aussi longtemps que je sois en âge
d’avoir cette analyse – et même avant), il se trouve que quasiment
toutes les politiques mises en œuvre dans notre pays semblent
s’évertuer à nous éloigner _radicalement_ de ce monde idéal. Alors
bien sûr, encore une fois, je n’oblige personne à avoir le même
rêve. Le principe démocratique voudrait que l’on confronte les
différents rêves de chacune et chacun pour trouver une voie médiane,
quelque chose qui pourrait satisfaire autant de monde que
possible. Sauf que j’ai quand même l’impression générale que le chemin
que nos dirigeants prennent ne correspond plus au rêve de grand monde
ici-bas.

_Malaise_.

Alors je me pose la question. Vous, politiciens, éditocrates, experts,
énarques – soyons francs, vous qui tenez les rênes : _quel est votre
rêve_ ?

### Votre rêve ?

Lorsque vous interrompez le mouvement (continu depuis plus d’un siècle)
de diminution du temps de travail par des « travailler plus pour gagner
plus » ou par l’augmentation de l’âge de départ à la retraite alors
qu’il n’y a déjà plus assez de travail pour tout le monde : quel est
votre but final ? Vers quoi nous emmenez-vous ? Quel sera votre limite ?

Lorsque vous continuez à prôner la croissance, c’est-à-dire
l’augmentation de la production de richesses chaque année, dans un monde
qui ne dispose déjà pas d’assez de ressources pour assurer durablement
la viabilité de la production actuelle : où est votre horizon ? Où
imaginez-vous nous conduire ?

Lorsque vous mettez en place des politiques de transport public qui
mènent à la réduction de moitié du transport ferroviaire du fret en 20
ans, compensée par l’augmentation des transports en camion ; lorsque
vous projetez également de fermer les petites lignes de chemin de fer
qui seront compensées par l’augmentation des transports en voitures
individuelles : quel est l’idéal derrière tout ça ? Quel horizon
écologique y voyez-vous ?

Lorsque vous prônez la privatisation des services publics qui impliquent
une perte de pouvoir politique généralisé pour le peuple : quelle
société souhaitez-vous créer ? Qui contrôlera notre destin collectif,
demain ?

Quel est votre rêve ?

On emmerde les candidats aux entretiens d’embauche avec des « où vous
voyez-vous dans dix ans ? », mais on ne prend même pas la peine de
demander aux gens qui ont le pouvoir dans ce pays (politique, mais aussi
médiatique ou économique) : « où voyez-vous votre pays dans cinquante
ans ? ». Où on sera ? Qu’est-ce qu’on fera ? Comment les gens occuperont
leurs journées ? Qu’est-ce qu’on aimerait avoir résolu comme problèmes
d’ici-là ? Dans quel monde on voudra vivre ?

Quel est votre rêve ?

### Notre cauchemar…

Je martèle la question, mais elle est presque rhétorique. En réalité,
j’ai bien l’impression que votre rêve est notre cauchemar, et que
c’est bien pour cela qu’il n’est jamais réellement formulé. Parce que
sa formulation claire et honnête serait trop scandaleuse et trop
écœurante pour être supportée par celles et ceux qui sont tenus de
vous donner une légitimité politique.

Votre rêve est celui du profit rapide et de la jouissance immédiate
exclusivement réservée à une élite : _vous_ et vos camarades de
_classe_, pour faire simple… et après vous, le déluge. Qu’importe si
l’immense majorité des gens sera perdante, qu’importe si l’humanité
entière sera perdante lorsque nous aurons achevé de rendre notre
planète invivable… vous serez morts depuis longtemps, et c’est là tout
votre « projeeeeeet ! » : vivre dans l’opulence égoïste, très vite,
tout de suite, pour ceux qui peuvent, et que les autres crèvent. Avant
que le château de cartes que vous aurez construit pour arriver à ce
« rêve » ne s’effondre.

Nulle part je ne vois de volonté politique de traitement social de la
misère ; nulle part je ne vois de volonté politique de traitement
économique de l’épuisement des ressources ; nulle part je ne vois de
volonté politique de combattre le mal-être et la perte de sens qui
gangrènent nos sociétés occidentales ; pour des gens qui se targuent
en permanence d’être « responsables » et « réalistes », ça se pose là.

Surtout, nulle part je ne ne vois d’horizon, de but humain et collectif
qui nous dépasserait.

Votre seul programme consiste à naviguer à vue, à nous engager de gré
ou de force tous dans votre bataille sans fin pour votre sacro-sainte
_croissance_, pour votre sacro-saint _emploi_… vous avez transformé
ces simples mesures qui ne devaient être que des moyens en des
objectifs intrinsèques, et tant pis si ces mesures n’ont alors plus
aucun sens[^reve12].

Tant pis s’il faudrait _justement_ remettre en cause ces moyens et
définir clairement, au-delà des moyens, _l’objectif_. Le rêve.

Mais non.

Après vous, le déluge.

Votre monde sans but s’effondrera de lui-même, j’en suis persuadé : les
« crises » (systémiques) économiques et humanitaires de ces vingt
dernières années n’en sont qu’un avant-goût. Je ne sais pas quand se
produira l’effondrement final et je sais pas non plus ce qui calanchera
en premier : l’équilibre écologique nécessaire à notre survie physique
ou l’équilibre social nécessaire à notre survie en tant que
civilisation. Mais votre monde s’effondrera, car il n’est soutenable ni
écologiquement ni humainement.

Et lorsque ce monde – dans lequel _nous_ sommes piégés – s’effondrera,
priez pour qu’il y ait suffisamment de rêveurs, de gens qui auront
cherché autre chose, qui auront _pensé_ autre chose, ces gens que vous
taxez d’_irréalistes_ et d’_irresponsables_, pour que les ruines de
votre monde ne soient pas le tombeau de l’humanité, pour que quelque
chose tienne dans le chaos.

Après vous, le déluge. En attendant, charge à nous autres, rêveurs, de
construire une arche.

[^reve1]: _There Is No Alternative_, il n'y a pas d'alternative (voir
    <https://fr.wikipedia.org/wiki/TINA>).

[^reve2]: Voir aussi _Inculture(s) 4 : Faim de pétrole_, conférence
    gesticulée écrite et interprêtée par Anthony Brault (disponible en
    vidéo sur le web).
    
[^reve3]: Voir aussi _LA PUBLICITÉ | Résistance à l'Agression
    Publicitaire_ sur le VLOG des Gens qui se Bougent (disponible en
    vidéo sur le web).

[^reve4]: Voir aussi _Violences de l’idéologie publicitaire_, par
    François Brune, publié en août 1995 dans _Le Monde diplomatique_.

[^reve5]: Voir aussi _Éloge de l'oisiveté_, conférence écrite et
    interprêtée par Dominique Rongvaux (disponible en vidéo sur le
    web).

[^reve6]: Voir aussi _Les secrets de la monnaie_, conférence écrite et
    interprêtée par Gérard Foucher (disponible en vidéo sur le web).

`LATEX` [^reve7]: Voir aussi _Duniter, comprendre la monnaie \u{G}_ :
`EPUB` [^reve7]: Voir aussi _Duniter, comprendre la monnaie Ğ1_ :
    <https://duniter.org/fr/comprendre/>

[^reve8]: Voir aussi _J'ai pas voté_, film documentaire de Moise
    Courilleau et Morgan Zahnd, sorti en septembre 2014 (disponible en
    vidéo sur le web).

[^reve9]: Voir aussi _J'ai pas voté – Le tirage au sort en politique_,
    addition au film documentaire de Moise Courilleau et Morgan Zahnd
    (disponible en vidéo sur le web).

[^reve10]: Voir aussi _Peut-on être communiste et objectif_, épisode
    d'_Ouvrez les guillemets_, émission présentée par Usul diffusée
    sur Mediapart (disponible en vidéo sur le web).
    
[^reve11]: Voir aussi _Bernard Friot et le salaire à vie_, épisode de
    _Mes chers contemporains_, émission présentée par Usul (disponible
    en vidéo sur le web).

[^reve12]: Voir aussi la _Loi de Goodhart_ :
    <https://fr.wikipedia.org/wiki/Loi_de_Goodhart>

# Quatrième de couverture

__Auteur en grève__

À l'occasion du mouvement national de grève du 5 décembre 2019 contre
la réforme des retraites en France, je vous propose ce livret qui
rassemble plusieurs de mes textes et bandes dessinées sur la
démocratie, la répartition des richesses, la pensée dominante,
l'effondrement et le rêve…

Aujourd'hui 5 décembre 2019, parce que cette énième attaque contre le
bien commun des classes laborieuses ne fait qu'ajouter une pierre à
l'édifice ignoble de la société capitaliste néolibérale que je conchie
article après article, je suis un auteur en grève.

Vive le socialisme.

Vive l'autogestion.

Vive la sécu.
        
* * *
            
__Gee__ est docteur en informatique (généraliste conventionné secteur
42), mais en dehors des heures de consultation, il est aussi auteur
dessinateur de bandes dessinées. Il publie de nombreux
« gribouillages » en ligne, parce que quitte à écrire des bêtises,
autant qu'elles soient lues !
