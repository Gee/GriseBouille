# Grise Bouille

Ce dépôt contient les sources de la bande dessinée
[Grise Bouille](https://grisebouille.net/) ainsi que de tout ce qui
gravite autour (livres, produits dérivés, etc.).

## Organisation générale

Les fichiers sont triés par type puis par contexte (blog, livre
papier, livre électronique, etc.). Un certain nombre de dossiers sont
vides (`png` par exemple) car ils sont destinés à recueillir les
fichiers de sortie générés à partir des sources.

Les sources et cibles sont en grande majorité organisés de manière
similaire (les sources des fichiers générés dans `png/bd_blog` sont
dans `svg/bd_blog`).

**Le répertoire le plus important est [svg/bd_blog](svg/bd_blog) qui
  contient les sources de base des BD publiées sur _Grise Bouille_**.
  
## Tous les répertoires

* `epub` : dossier où sont générés les livres électroniques au format
  EPUB ;
- `jpg/a5` : réalisations graphiques sur papier numérisées ;
  (aquarelles, etc.) au format a5 noir et blanc pour livres
  électroniques ;
- `jpg/blog` : réalisations graphiques sur papier numérisées
  (aquarelles, etc.) au format blog ;
- `jpg/boutique` : réalisations graphiques sur papier numérisées
  (aquarelles, etc.) pour produits dérivés ;
-  `latex` : ddossier où sont stockés les modèles et styles LaTeX
  et où sont générées les sources LaTeX des livres électroniques au
  format PDF ;
- `markdown/livres` : les sources de base des livres Grise Bouille ;
* `pdf` : dossier où sont générés les livres électroniques au format PDF ;
* `png/bd_a5_90dpi` : dossier où sont générées les images pour le format A5 PDF électronique ;
* `png/bd_a5_300dpi` : dossier où sont générées les images haute définition pour le format A5 imprimé ;
* `png/bd_blog` : dossier où sont générées les images pour le blog ;
* `png/bd_miniature` : dossier pour stocker les miniatures pour les BD
  publiées sur Framablog (découpées à la main au cas par cas) ;
* `png/bd_paysage` : dossier où sont générées les images au format paysage adapté aux liseuses ;
* `png/blog` : dossier où sont générées les images du thème graphique du blog ;
* `png/boutique` : dossier où sont générées les images utilisées sur les produits dérivés ;
* `png/communication` : dossier où sont générées les images utilisées pour la communication autour de _Grise Bouille_ ;
* `png/couvertures` : dossier où sont générées les couvertures des livres ;
- `scripts` : les différents scripts qui automatisent certains processus dans les chaînes de publication de _Grise Bouille_ ;
- `svg/bd_a5` : les sources SVG des BD au format A5 pour impression et PDF ;
- `svg/bd_blog` : les sources SVG des BD au format blog (format d'origine) ;
- `svg/bd_paysage` : les sources SVG des BD au format paysage adapté aux liseuses ;
- `svg/blog` : les sources SVG des images du thème graphique du blog ;
- `svg/boutique` : les sources SVG des images utilisées sur les produits dérivés ;
- `svg/communication` : les sources SVG des images utilisées pour la communication autour de _Grise Bouille_ ;
- `svg/couvertures` : les sources SVG des couvertures des livres ;
* `tmp` : dossier temporaire utilisé par les scripts ;
- `xhtml` : dossier où sont stockés les modèles et styles EPUB et où sont générées les sources XHTML des livres électroniques au format EPUB.
  
## Fonctionnements

### Le blog

Le format original des BD est le format blog : le produit fini est une
image PNG de 800 pixels de large et de hauteur variable. Les images
PNG sont générées « à la main » depuis _Inkscape_ (fonction exporter)
en exportant la page avec une largeur de 800 pixels (la hauteur est
calculée automatiquement par _Inkscape_).

### Les livres

Les livres sont générés de manière semi-automatique. Les deux seules
étapes non-automatiques sont les suivantes :

1. écriture du texte et du contenu du livre dans un format markdown
   avec quelques balises persos (voir dans `markdown/livres`) ;
2. remise en page des BD depuis le format blog vers un format A5 (pour
   les livres papier et numériques PDF) ou un format paysage (pour les
   livres numériques EPUB). Cette étape consiste à définir une grille
   dans _Inkscape_ dont les dimensions correspondent à la taille d'une
   page (A5 ou paysage), puis à déplacer les différents dessins et
   blocs de textes de la BD pour que rien ne soit coupé par un
   changement de page (et pour que les pages soient uniformément
   remplies dans la mesure du possible).
   
La suite est réalisée automatiquement par un jeu de scripts :

* `scripts/generer_sources_a5_300dpi.rb` génère le fichier LaTeX de base qui
  servira à générer le PDF de la version papier. Il génère également
  les images PNG de chaque article à partir de la source SVG, en
  découpant chaque article selon la grille (contrairement à la version
  blog, ici, un SVG est exportée en plusieurs PNG selon le nombre de
  pages) ;
* `scripts/generer_sources_a5_90dpi.rb` réalise la même opération pour le PDF
  numérique ;
* `scripts/generer_sources_paysage.rb` génère le contenu du fichier
  EPUB au format XHTML (ainsi que les fichiers nécessaires comme
  `content.opf`, etc.);
* `scripts/generer_a5_300dpi.rb` compile le fichier LaTeX vers un PDF
  destiné à l'impression ;
* `scripts/generer_a5_90dpi.rb` compile le fichier LaTeX vers un PDF
  destiné à la lecture sur écran.
* `scripts/generer_paysage.rb` compile les fichiers XHTML vers un EPUB
  destiné à la lecture sur liseuse.

Ces trois derniers scripts appellent le script nécessaire de
génération de sources correspondant s'ils ne trouvent pas les
sources.

Ces scripts n'ont été testé que sous Gnunux (et ne fonctionnent
probablement pas en dehors) et ont les dépendances suivantes :

* `ruby` avec les composants `date` et `i18n`
* `inkscape`
* `imagemagick`
* `pandoc`
* `texlive-full`
* `zip`
* `epubcheck`


## Quelques notes

* _Grise Bouille_ est sous licence libre mais n'est en aucun cas une
  BD collaborative, ce dépôt n'accepte donc pas de contribution
  (_merge request_). Vous êtes libres de le forker pour faire votre
  version dérivée, bien entendue (en respectant les termes de la
  licence `CC-By-Sa`) ;

* Vous pouvez poster des _issues_ si vous le souhaitez, que ce soit
  pour suggérer quelque chose ou pour signaler une faute
  d'orthographe, par exemple. Je répète, par contre, que cette BD
  n'est pas collaborative, je ne dessine donc pas les scénarios
  d'autrui et je ne prends donc pas les suggestions de ce genre.
