# Informations légales

_METATITRE_ par Simon « Gee » Giraudot est mis à disposition selon les
termes de la licence Creative Commons Paternité - Partage des
Conditions Initiales à l’Identique 2.0 France.

[grisebouille.net](https://grisebouille.net/)

Cette licence s’applique à l’intégralité de ce présent document. Pour
plus de détails, je vous invite à faire de plus amples recherches sur
Internet concernant la licence
[CC-by-sa](https://creativecommons.org/licenses/by-sa/2.0/fr/).

![](images/Logo_CC_by_SA.png)

Pour faire simple, ce document est libre, vous avez donc le droit de le
partager, de le vendre, de le modifier, d’en extraire des parties, de le
réutiliser, bref, d’en faire absolument tout ce que vous voulez !

Il y a seulement 2 conditions obligatoires pour cela :

- toujours préciser la paternité de l’œuvre. Dans le cas présent, la
  paternité doit être attribuée à Simon « Gee » Giraudot, avec une
  référence au site [grisebouille.net](https://grisebouille.net) ;
    
- diffuser cette œuvre ou toute œuvre dérivée (ou utilisant une partie
  de cette œuvre) sous la même licence, avec les mêmes droits et les
  mêmes conditions d’utilisation. Histoire que tout cela reste
  toujours libre.

À diffuser et à partager autant que vous le souhaitez !

_Vive la culture libre !_
