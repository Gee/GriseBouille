#!/usr/bin/ruby
# coding: utf-8

require_relative 'common'

conf = Config.new(ARGV)

factor = 0.96 / 0.9

if conf.id != ""

  filenames = Dir["#{conf.source_dir}/#{conf.id}*.svg"]

  filenames.sort!

  page_id = 0

  filenames.each do |filename|
    puts filename
    
    version = `grep "inkscape:version" #{filename}`.split("\"")[1].split(" ")[0].to_f

    height_str = `grep \"height=\\\"\" #{filename} -m 1`

    height = height_str.split("\"")[1]
    if height_str.include?("cm")
      height = height.chop.chop.to_f
    else
      height = height.to_f / (0.393700790 * 96)
    end

    if version < 0.92
      nb_pages = (height.to_f / conf.legacy_page_height).round.to_i
    elsif conf.id.start_with?("gknd")
      nb_pages = (height.to_f / conf.legacy_img_height).round.to_i
    else
      nb_pages = (height.to_f / conf.img_height).round.to_i
    end

    puts nb_pages
    
    offset_px = 0
    img_width = conf.img_width
    img_height = conf.img_height

    if conf.id.start_with?("gknd") # Règle spéciale GKND fait avec ancienne mesures
      img_width = conf.legacy_img_width
      img_height = conf.legacy_img_height
    end
    
    if version < 0.92 # Gestion des conneries de pu#ø~€n d'Inkscape legacy 90dpi
      if height_str.include?("cm") # si height en cm, page != dessin
        # On calcule le décalage entre l'image et le bas de la page
        offset_cm = height.to_f - (height.to_f / factor)
        offset_px = offset_cm * 0.393700790 * 96
        puts offset_px
      else # si height en px, ok
        offset_px = 0
      end
      img_width = conf.legacy_img_width
      img_height = conf.legacy_img_height
    end
    
    for i in 0..(nb_pages-1) do
      if page_id < 10
        name = conf.target_dir + "/" + conf.id + "_0" + page_id.to_s + ".png"
      else
        name = conf.target_dir + "/" + conf.id + "_" + page_id.to_s + ".png"
      end

      # On exporte la bonne zone
      x1 = (img_width * 0.393700790 * 96).round.to_i
      y0 = (offset_px + ((nb_pages - 1 - i) * img_height) * 0.393700790 * 96).round.to_i
      y1 = (offset_px + ((nb_pages - i) * img_height) * 0.393700790 * 96).round.to_i

      system "inkscape -a 0:#{y0}:#{x1}:#{y1} -e #{name} -w #{conf.width} #{filename}"

      if conf.paysage
        system "mogrify -colorspace Gray -rotate -90 #{name}"
      else
        system "mogrify -colorspace Gray #{name}"
      end

      page_id = page_id + 1
    end
  end
end
