#!/usr/bin/ruby
# coding: utf-8

require 'date'
require_relative 'common'

if ARGV.empty?
  puts "Usage: generer_sources_livre.rb [id_livre]"
  exit
end

conf = Config.new(ARGV)
metadata = conf.get_metadata

if conf.paysage

  if !is_output_up_to_date("xhtml/#{conf.id}_content.opf", "markdown/livres/#{conf.id}.md")
    system "ruby scripts/internal/generer_sources_livre.rb #{conf.mode} #{conf.id}"
  end

  system "rm -rf tmp/#{conf.id}_epub"
  system "mkdir tmp/#{conf.id}_epub"
  system "mkdir tmp/#{conf.id}_epub/META-INF"
  system "mkdir tmp/#{conf.id}_epub/OPS"
  system "mkdir tmp/#{conf.id}_epub/OPS/png"
  system "mkdir tmp/#{conf.id}_epub/OPS/jpg"
  system "cp epub/template/container.xml tmp/#{conf.id}_epub/META-INF"
  
  current_dir = `pwd`.chop
  
  system "ln -s #{current_dir}/xhtml/template/Logo_CC_by_SA.png tmp/#{conf.id}_epub/OPS/png/Logo_CC_by_SA.png"
  system "ln -s #{current_dir}/xhtml/template/stylesheet.css tmp/#{conf.id}_epub/OPS/stylesheet.css"
  system "ln -s #{current_dir}/png/couvertures/#{conf.id}.png tmp/#{conf.id}_epub/OPS/png/couv.png"
  
  Dir["xhtml/#{conf.id}_*"].each do |f|
    new_file = f.split("#{conf.id}_")[1]
    puts "cp #{f} tmp/#{conf.id}_epub/OPS/#{new_file}"
    system "cp #{f} tmp/#{conf.id}_epub/OPS/#{new_file}"
  end

  content = File.read("tmp/#{conf.id}_epub/OPS/content.opf")

  content.each_line do |l|
    if !l.include?("<item id=")
      next
    end

    file = l.split("href=\"")[1].split("\"")[0]

    if file.include?("png")
      filename = "png/bd_paysage/" + file.split("png/")[1]
    elsif file.include?("jpg")
      filename = "jpg/a5/" + file.split("jpg/")[1]
    else
      next
    end
    system "ln -s #{current_dir}/#{filename} tmp/#{conf.id}_epub/OPS/#{file}"
  end
  
  system "rm -r epub/#{conf.id}.epub"
  system "zip -j -X -Z store epub/#{conf.id}.epub epub/template/mimetype"
  system "cd tmp/#{conf.id}_epub/ && zip -r ../../epub/#{conf.id}.epub META-INF OPS"
  
# epubcheck
  system "epubcheck epub/#{conf.id}.epub"
  
else
  
  latex_filename = "#{conf.id}_300dpi.tex"
  if conf.a5_90dpi
    latex_filename = "#{conf.id}_90dpi.tex"
  elsif conf.paysage
    latex_filename = ""
  end

  if !is_output_up_to_date("latex/#{latex_filename}", "markdown/livres/#{conf.id}.md")
    system "ruby scripts/internal/generer_sources_livre.rb #{conf.mode} #{conf.id}"
  end

  system "cd latex && pdflatex -output-directory ../pdf #{latex_filename} && pdflatex -output-directory ../pdf #{latex_filename}"

end
