#!/usr/bin/ruby
# coding: utf-8

require 'date'
require_relative 'common'

choices = Dir.glob("markdown/livres/*.md")

choices.sort!


puts "ID disponibles:"

choices.each do |c|
  if c.include?("README")
    next
  end
  
  title = `grep \"TITRE = \" #{c}`.split("TITRE = ").last
  puts " * " + c.split("/").last.split(".md").first + " - " + title
end
