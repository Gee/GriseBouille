#!/usr/bin/ruby
# coding: utf-8

require 'date'
require_relative 'common'

# On gère à la main l'éventuel italique dans les titres
def preprocess(title)
  if title.include?("_")
    title.sub!("_","\\emph{").sub!("_","}")
  end
  return title
end

# Fonction qui écrit le bon fichier XHTML au bon endroit et avec les
# bonnes infos
def write_xhtml(content_md, id, title, tag, matter)
  if content_md == ""
    return
  end
  pandoc_input = File.open("tmp/pandoc_input.md", "w")
  pandoc_input.write(content_md)
  pandoc_input.close
  pandoc_output = `pandoc tmp/pandoc_input.md -t html`

  content_xhtml = File.read("xhtml/template/page.xhtml")
  content_xhtml.gsub!("METATITRE", title)
  content_xhtml.gsub!("METAMATTER", matter)
  content_xhtml.gsub!("METACONTENT", pandoc_output)

  output = File.open("xhtml/#{id}_#{tag}.xhtml", "w")
  output.write(content_xhtml)
end

if ARGV.empty?
  puts "Usage: generer_sources_livre.rb [id_livre]"
  exit
end

conf = Config.new(ARGV)
metadata = conf.get_metadata

if !metadata.has_key?("ISBN")
  puts "Erreur: pas d'ISBN trouvé dans #{conf.id}"
  exit
end

if conf.paysage

  # Couverture, infos légales
  content_xhtml = File.read("xhtml/template/cover.xhtml")
  content_xhtml.gsub!("METATITRE", metadata["TITRE"])
  output = File.open("xhtml/#{conf.id}_cover.xhtml", "w")
  output.write(content_xhtml)

  content_xhtml = File.read("xhtml/template/infos-legales.xhtml")
  content_xhtml.gsub!("METATITRE", metadata["TITRE"])
  output = File.open("xhtml/#{conf.id}_infos-legales.xhtml", "w")
  output.write(content_xhtml)

  # Les items fixes
  items = Hash.new
  items["Logo_CC_by-SA"] = "png/Logo_CC_by_SA.png"
  items["couv"] = "png/couv.png"
  items["nav"] = "nav.xhtml"
  items["cover"] = "cover.xhtml"
  items["infos-legales"] = "infos-legales.xhtml"
  items["ncx"] = "toc.ncx"
  items["stylesheet"] = "stylesheet.css"
  
  itemrefs = Hash.new
  itemrefs["cover"] = "Couverture"
  itemrefs["infos-legales"] = "Informations légales"
else
  
  # Génération header

  # Les premiers tomes ont une ancienne version du header
  # if conf.id == "gb01" || conf.id == "gb02"
  #   content = File.read("latex/template/header.tex")
  # elsif conf.id.start_with?("gknd")
  #   content = File.read("latex/template/header_v3.tex")
  # elsif conf.id.start_with?("greve")
  #   content = File.read("latex/template/header_no_frama.tex")
  # else
  #   content = File.read("latex/template/header_v2.tex")
  # end
  content = File.read("latex/template/header_ptilouk.tex")

  # On remplie les bonnes infos
  content.gsub!("METAANNEE", Date.parse(metadata["PUBLICATION"]).year.to_s)
  content.gsub!("METATITRE", metadata["TITRE_A"])
  content.gsub!("METASOUSTITRE", metadata["TITRE_B"])
  content.gsub!("METAISBN", metadata["ISBN"])
  content.gsub!("METAPRIX", metadata["PRIX"])
  content.gsub!("METADEPOTLEGAL", metadata["DEPOT_LEGAL"])
  content.gsub!("METACOUVERTURE", metadata["PHOTOGRAPHIE"])

  if conf.a5_300dpi
    puts "300dpi"
    content.gsub!("METADPI", "300dpi")
  elsif conf.a5_90dpi
    puts "90dpi"
    content.gsub!("METADPI", "90dpi")
  end

end

# Prétraitement markdown perso
content_md = ""

# Génération fichiers XHTML pour l'EPUB
if conf.paysage
  current_title = ""
  current_tag = ""
  matter = "frontmatter"
else
  # Si le livre a une dédicace sur la 1nd page, on la met
  if metadata.has_key?("DEDICACE")
    content_md = content_md + "\n\n\\newpage\n\n\\thispagestyle{empty}\n\n"
    content_md = content_md + "\\vspace*{4cm}\n\n\\begin{flushright}\n\n"
    content_md = content_md + "#{metadata["DEDICACE"]}\n\n"
    content_md = content_md + "\\end{flushright}\n\n"
  end
end

metadata["CONTENT"].each_line do |l|

  # Traiter lignes spéciales pour LaTeX ou EPUB
  if l.start_with?("\`LATEX\` ")
    if conf.paysage
      next
    end
    l = l.split("\`LATEX\` ")[1]
    if l == "SKIP_PAGE"
      skip_page = true
      next
    end
  elsif l.start_with?("\`EPUB\` ")
    if !conf.paysage
      next
    end
    l = l.split("\`EPUB\` ")[1]
  end

  if l.start_with?("# ")
    ############
    ## PARTIE ##
    ############
    
    title = preprocess(l.split("# ")[1].chop)
    tag = make_tag_from_name(title)

    if conf.paysage # XHTML
      title.gsub!("&","&amp;")
      write_xhtml(content_md, conf.id, current_title, current_tag, matter)
      if title == "Quatrième de couverture"  # Quatrième de couv en EPUB
        content_md = ""
        tag = "qtre_couv"
      else
        content_md = "<div><img alt=\"\" src=\"png/#{tag}_00.png\" /></div>\n\n"
        items["#{tag}_00"] = "png/#{tag}_00.png"
      end

      items[tag] = "#{tag}.xhtml"
      itemrefs[tag] = title

      current_title = title
      current_tag = tag
      matter = "bodymatter"
    else # LATEX
      title.gsub!("&","\\\\&")
      if title == "Quatrième de couverture"  # Pas de quatrième de couv en LaTeX
        break
      end
      content_md = content_md + "\\mypart{" + title + "}\n\n"
      content_md = content_md + "\\label{" + tag + "}\n\n"
      content_md = content_md + "\\illustration{" + tag + "_00}\n\n"
      content_md = content_md + "\\newpage\n\\thispagestyle{empty}\n\n"
    end

    if tag == "qtre_couv"
      next
    end
    
    # Generation de l'image de partie
    if !is_output_up_to_date("#{conf.target_dir}/#{tag}_00.png", "#{conf.source_dir}/#{tag}.svg")
      puts "[Génération des images de #{tag}]"
      system "ruby scripts/internal/exporter_a5_et_paysage.rb #{conf.mode} #{tag}"
    end
    
  elsif l.start_with?("## ")
    
    if l.include?("[")
      ########
      ## BD ##
      ########

      title = preprocess(l.split("# ")[1].split("[")[0].chop)
      small_title = ""
      if title.include?("|")
        small_title = title.split("|")[0] + "…"
        title.gsub!("|","")
      end

      # Hack pour pouvoir sauter une page au besoin
      tag = l.split("[")[1].split("]")[0]
      skip_page = false
      if l.split("] ")[1] == "SKIPPAGE\n"
        skip_page = true
      end
      
      if conf.paysage # XHTML
        title.gsub!("&","&amp;")
        write_xhtml(content_md, conf.id, current_title, current_tag, matter)
        items[tag] = "#{tag}.xhtml"
        itemrefs[tag] = title
        content_md = ""
        current_title = title
        current_tag = tag
        if !tag.include?("_") # Article texte
          content_md = "<h1><span class=\"titre\">" + title + "</span></h1>\n"
        end
      else # LATEX
        title.gsub!("&","\\\\&")
        if small_title == ""
          content_md = content_md + "\\mychapter{" + title + "}\n\n"
        else
          small_title.gsub!("&","\\\\&")
          content_md = content_md + "\\mylongchapter{" + title + "}{" + small_title + "}\n\n"
        end
        if !tag.include?("_") # Article texte
          content_md = content_md + "\\chapter*{" + title + "}\n\n"
        end
        content_md = content_md + "\\label{" + tag + "}\n\n"
      end

      # Generation des images
      if tag.start_with?("tad") # image unique JPG
        if conf.paysage # XHTML
          content_md = content_md + "<div><img alt=\"\" src=\"jpg/#{tag}.jpg\" /></div>\n"
          items["#{tag}_img"] = "jpg/#{tag}.jpg"
        else # LATEX
          content_md = content_md + "\\illustrationjpg{" + tag + "}\n\n"
        end
      elsif File.exist?("#{conf.source_dir}/#{tag}.svg") || File.exist?("#{conf.source_dir}/#{tag}_1.svg")
        # plusieurs images PNG

        if !is_output_up_to_date("#{conf.target_dir}/#{tag}_00.png", "#{conf.source_dir}/#{tag}.svg") || !is_output_up_to_date("#{conf.target_dir}/#{tag}_00.png", "#{conf.source_dir}/#{tag}_1.svg")
          puts "[Génération des images de #{tag}]"
          system "ruby scripts/internal/exporter_a5_et_paysage.rb #{conf.mode} #{tag}"
        end

        if skip_page
          content_md = content_md + "\\illustrationempty\n\n"
          content_md = content_md + "\\newpage\n\n"
          content_md = content_md + "\\thispagestyle{empty}\n\n"
          skip_page = false
        end

        # On inclue toute les images
        nb_img = number_of_images(tag, "#{conf.target_dir}")
        for i in 0..(nb_img-1) do
          if i < 10
            if conf.paysage # XHTML
              content_md = content_md + "<div><img alt=\"\" src=\"png/#{tag}_0#{i.to_s}.png\" /></div>\n"
              items["#{tag}_0#{i.to_s}"] = "png/#{tag}_0#{i.to_s}.png"
            else # LATEX
              content_md = content_md + "\\illustration{" + tag + "_0" + i.to_s + "}\n\n"
            end
          else
            if conf.paysage # XHTML
              content_md = content_md + "<div><img alt=\"\" src=\"png/#{tag}_#{i.to_s}.png\" /></div>\n"
              items["#{tag}_#{i.to_s}"] = "png/#{tag}_#{i.to_s}.png"
            else # LATEX
              content_md = content_md + "\\illustration{" + tag + "_" + i.to_s + "}\n\n"
            end
          end
        end
      end
      
    else      
      #############################
      ## PRÉFACE ET AVANT-PROPOS ##
      #############################

      title = preprocess(l.split("## ")[1].chop)

      if conf.paysage # XHTML
        tag = make_tag_from_name(title)
        write_xhtml(content_md, conf.id, current_title, current_tag, matter)
        items[tag] = "#{tag}.xhtml"
        itemrefs[tag] = title
        current_title = title
        current_tag = tag
        content_md = "<h1><span class=\"titre\">#{title}</span></h1>\n"
      else # LATEX
        content_md = content_md + "\\cleardoublepage\n\n"
        content_md = content_md + "\\phantomsection\n\n"
        content_md = content_md + "\\chapter*{" + title + "}\n"
        content_md = content_md + "\\addcontentsline{toc}{chapter}{" + title + "}\n"
      end
      
    end
    
  elsif l.start_with?("### ")
    ########################
    ## SOUS-TITRE PRÉFACE ##
    ########################
    title = preprocess(l.split("### ")[1].chop)
    
    if conf.paysage # XHTML
      tag = make_tag_from_name(title)
      content_md = content_md + "<h2 id=\"#{tag}\" class=\"unnumbered\">#{title}</h2>\n"
    else # LATEX
      content_md = content_md + "\\section*{" + title + "}\n"
    end

  elsif l.start_with?("– ")
    ####################################
    ## SIGNATURE PRÉFACE AVANT-PROPOS ##
    ####################################

    if conf.paysage # XHTML
      content_md = content_md + l
    else # LATEX
      content_md = content_md + "\\bigskip\n\n\\begin{flushright}\n\n"
      if l.start_with?("– ")
        content_md = content_md + "-- " + l.split("– ")[1]
      elsif l.start_with?("_Article publié le")
        content_md = content_md + "\\emph{" + l.split("_")[1] + "}\n"
      end
      content_md = content_md + "\n\\end{flushright}\n\n\\vfill\n"
    end
    
    if l == "– Gee\n"
      ########################
      ## FIN DU FRONTMATTER ##
      ########################

      if conf.paysage # XHTML

      else # LATEX
        content_md = content_md + "\n\\mainmatter\n"
      end
      
    end
    
  elsif l.start_with?("_Article publié le")
    #########################
    ## DATE DE PUBLICATION ##
    #########################

    if conf.paysage # XHTML
      content_md = content_md + l
    else # LATEX
      content_md = content_md + "\\begin{flushright}\n\n"
      if l.start_with?("– ")
        content_md = content_md + "-- " + l.split("– ")[1]
      elsif l.start_with?("_Article publié le")
        content_md = content_md + "\\emph{" + l.split("_")[1] + "}\n"
      end
      content_md = content_md + "\n\\end{flushright}\n\n\\bigskip\n"
    end
    
  else
    ###################
    ## LIGNE NORMALE ##
    ###################

    content_md = content_md + l
          
  end
end

if conf.paysage
  write_xhtml(content_md, conf.id, current_title, current_tag, matter)

  # Generer content.opf
  item_string = ""
  items.each do |key, value|

    if key == "nav"
      item_string = item_string + "   <item id=\"#{key}\" href=\"#{value}\" properties=\"nav\" media-type=\""
    else
      item_string = item_string + "   <item id=\"#{key}\" href=\"#{value}\" media-type=\""
    end

    ext = value.split(".")[1]
    if ext == "png"
      item_string = item_string + "image/png"
    elsif ext == "xhtml"
      item_string = item_string + "application/xhtml+xml"
    elsif ext == "ncx"
      item_string = item_string + "application/x-dtbncx+xml"
    elsif ext == "css"
      item_string = item_string + "text/css"
    elsif ext == "jpg"
      item_string = item_string + "image/jpeg"
    end
    item_string = item_string + "\" />\n"
  end
  
  itemref_string = ""
  itemrefs.each_key do |e|
    itemref_string = itemref_string + "   <itemref idref=\"#{e}\" />\n"
  end

  content_xhtml = File.read("xhtml/template/content.opf")
  content_xhtml.gsub!("METATITRE", metadata["TITRE"])
  content_xhtml.gsub!("METANNEE", Date.parse(metadata["PUBLICATION"]).year.to_s)
  content_xhtml.gsub!("METADATE", metadata["PUBLICATION"])
  content_xhtml.gsub!("METAITEMS", item_string.chop)
  content_xhtml.gsub!("METAITEMREFS", itemref_string.chop)
  output = File.open("xhtml/#{conf.id}_content.opf", "w")
  output.write(content_xhtml)

  # Generer nav et toc

  nav_string = ""
  toc_string = ""

  nav_point = 0
  level = 6
  level_toc = 4
  
  itemrefs.each do |tag, title|
    if(tag == "cover")
      next
    end

    nav_point = nav_point + 1

    part = (tag == "comic_trip" || tag == "tu_sais_quoi" || tag == "depeches_melba" || tag == "la_fourche" || tag == "table_a_dessins" || tag == "qtre_couv" || tag == "democratie__representation" || tag == "repartition_des_richesses" || tag == "pensee_dominante" || tag == "effondrement__renaissance")

    file = items[tag]

    if part && tag != "comic_trip" && tag != "democratie__representation" && !conf.id.start_with?("gknd")
      level = level - 2
      indent = ""
      level.times { indent = indent + " " }
      nav_string = nav_string + "#{indent}</ol>\n"
      level = level - 2
      indent = ""
      level.times { indent = indent + " " }
      nav_string = nav_string + "#{indent}</li>\n"

      level_toc = level_toc - 2
      indent = ""
      level_toc.times { indent = indent + " " }
      toc_string = toc_string + "#{indent}</navPoint>\n"
    end
    
    indent = ""
    level.times { indent = indent + " " }

    nav_string = nav_string + "#{indent}<li><a href=\"#{file}\">#{title}</a>"

    indent = ""
    level_toc.times { indent = indent + " " }
    
    toc_string = toc_string + "#{indent}<navPoint id=\"NavPoint-#{nav_point}\" playOrder=\"#{nav_point}\">\n"
    toc_string = toc_string + "#{indent}  <navLabel>\n"
    toc_string = toc_string + "#{indent}    <text>#{title}</text>\n"
    toc_string = toc_string + "#{indent}  </navLabel>\n"
    toc_string = toc_string + "#{indent}  <content src=\"#{file}\" />\n"
    
    if part
      if tag == "qtre_couv"
        nav_string = nav_string + "</li>\n"
      level = level - 2
      indent = ""
      level.times { indent = indent + " " }

      indent = ""
      level_toc.times { indent = indent + " " }
      toc_string = toc_string + "#{indent}</navPoint>\n"
      else
        nav_string = nav_string + "\n"
        level = level + 2
        indent = ""
        level.times { indent = indent + " " }
        nav_string = nav_string + "#{indent}<ol>\n"
        level = level + 2

        level_toc = level_toc + 2
      end
    else
      nav_string = nav_string + "</li>\n"
      
      indent = ""
      level_toc.times { indent = indent + " " }
      toc_string = toc_string + "#{indent}</navPoint>\n"
    end
  end

  content_xhtml = File.read("xhtml/template/nav.xhtml")
  content_xhtml.gsub!("METACONTENT", nav_string.chop)
  output = File.open("xhtml/#{conf.id}_nav.xhtml", "w")
  output.write(content_xhtml)

  content_xhtml = File.read("xhtml/template/toc.ncx")
  content_xhtml.gsub!("METATITRE", metadata["TITRE"])
  content_xhtml.gsub!("METACONTENT", toc_string.chop)
  output = File.open("xhtml/#{conf.id}_toc.ncx", "w")
  output.write(content_xhtml)

else
  # Conversion contenu en LaTeX via pandoc
  pandoc_input = File.open("tmp/pandoc_input.md", "w")
  pandoc_input.write(content_md)
  pandoc_input.close
  pandoc_output = `pandoc tmp/pandoc_input.md -t latex --top-level-division=chapter`.gsub("⋅","{\\textperiodcentered}").gsub("\\emph","\\textit")

  footer = "\n\\backmatter\n\n\\tableofcontents\n\n\\end\{document\}\n"

  content = content + pandoc_output + footer

  # Écriture du résultat

  if conf.a5_300dpi
    output = File.open("latex/#{conf.id}_300dpi.tex", "w")
    output.write(content)
  elsif conf.a5_90dpi
    output = File.open("latex/#{conf.id}_90dpi.tex", "w")
    output.write(content)
  end
end
