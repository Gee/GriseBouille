#!/usr/bin/ruby
# coding: utf-8

require "i18n"

class Config
  def initialize(args)
    @mode = "--a5-300dpi"
    @id = ""
    args.each do |a|
      if a == "--a5-90dpi" || a == "--a5-300dpi" || a == "--paysage"
        @mode = a
      else
        @id = a
      end
    end

    if @mode == "--a5-300dpi"
      @source_dir = "svg/bd_a5"
      @target_dir = "png/bd_a5_300dpi"
      @width = 1217
      @height = 1970
      @img_width = 33
      @img_height = 53.42
      @legacy_img_width = 31.5
      @legacy_img_height = 51
      @legacy_page_height = 54.4
    elsif @mode == "--a5-90dpi"
      @source_dir = "svg/bd_a5"
      @target_dir = "png/bd_a5_90dpi"
      @width = 730
      @height = 1182
      @img_width = 33
      @img_height = 53.42
      @legacy_img_width = 31.5
      @legacy_img_height = 51
      @legacy_page_height = 54.4
    elsif mode == "--paysage"
      @source_dir = "svg/bd_paysage"
      @target_dir = "png/bd_paysage"
      @width = 1360
      @height = 1020
      @img_width = 33
      @img_height = 26.4
      @legacy_img_width = 31.5
      @legacy_img_height = 23.625
      @legacy_page_height = 25.2
    end
  end

  def mode
    return @mode
  end

  def a5_300dpi
    return (@mode == "--a5-300dpi")
  end

  def a5_90dpi
    return (@mode == "--a5-90dpi")
  end

  def paysage
    return (@mode == "--paysage")
  end

  def id
    return @id
  end
  
  def source_dir
    return @source_dir
  end

  def target_dir
    return @target_dir
  end

  def width
    return @width
  end

  def height
    return @height
  end

  def legacy_img_width
    return @legacy_img_width
  end

  def legacy_img_height
    return @legacy_img_height
  end

  def legacy_page_height
    return @legacy_page_height
  end

  def img_width
    return @img_width
  end

  def img_height
    return @img_height
  end
  
  def get_metadata
    filename = "markdown/blog/#{@id}.md"

    if !File.exist?(filename)
      filename = "markdown/livres/#{@id}.md"
      if !File.exist?(filename)
        puts "Erreur: aucune entrée ne correspond au tag #{@id}"
        exit
      end
    end

    file = File.new(filename, "r")
    map = Hash.new

    content = ""
    in_content = false
    line = file.gets # remove first line
    while line = file.gets

      if !in_content
        if !line.include?(" = ")
          in_content = true
          next
        end
        line = line.split(" = ")
        map[line[0]] = line[1].chop
      else
        content = content + line
      end
    end
    file.close

    map["CONTENT"] = content
    return map
  end
end


def is_output_up_to_date(output, input)
  if !File.exist?(input)
    return true
  end
  
  if !File.exist?(output)
    return false
  end
  toutput = File.mtime(output)
  tinput = File.mtime(input)

  return toutput >= tinput
end

def make_tag_from_name(name)
  I18n.config.available_locales = :en
  return I18n.transliterate(name).downcase.gsub(" ", "_").gsub(/[^0-9A-Za-z_]/, '')
end

def number_of_images(id_bd, directory)
   return Dir[directory + "/" + id_bd + "*.png"].size
end
  
