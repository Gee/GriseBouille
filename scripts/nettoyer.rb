#!/usr/bin/ruby
# coding: utf-8

system "rm -rf latex/*.tex latex/\#*"
system "rm -rf png/bd_a5_*/*.png png/bd_paysage/*.png"
system "mv tmp/README.md /tmp"
system "rm -rf tmp/*"
system "mv /tmp/README.md tmp/"
system "rm -rf xhtml/*.xhtml xhtml/*.opf xhtml/*.ncx"
