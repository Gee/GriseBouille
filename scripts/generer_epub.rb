#!/usr/bin/ruby

if ARGV.size != 1
  puts "Usage: ruby script/generer_epub.rb [id_livre]"
  system "ruby scripts/internal/afficher_id_disponibles.rb"
  exit
end

system "ruby scripts/internal/generer_livre.rb --paysage #{ARGV[0]}"
