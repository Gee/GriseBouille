#!/usr/bin/ruby

if ARGV.size != 1
  puts "Usage: ruby script/generer_a5_90dpi.rb [id_livre]"
  system "ruby scripts/internal/afficher_id_disponibles.rb"
  exit
end

system "ruby scripts/internal/generer_livre.rb --a5-90dpi #{ARGV[0]}"
