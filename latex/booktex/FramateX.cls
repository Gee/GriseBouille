% Class FramateX inspirée très fortement par la framaquette
% de Vincent Lozano & de Didier Roche, retravaillée par
% David Dauvergne (la Poule ou l'Oeuf) puis par
% Christophe Masutti (Framasoft)

\newcommand{\pouleVersion}{0.1}
\NeedsTeXFormat{LaTeX2e}
\ProvidesClass{FramateX}[2008/11/06]

%-----------------------------------------------------------------------
%
% Avec des si...
%
\newif\ifversionenligne
%
\versionenlignetrue
\newif\ifminisommaire
\minisommairetrue
%

\newif\if@versionreport \@versionreportfalse




%-----------------------------------------------------------------------
% encodage
\RequirePackage[utf8]{inputenc}
%-----------------------------------------------------------------------
%
% Declaration des options
%
\DeclareOption{draft}{\PassOptionsToClass{draft}{book}}
\DeclareOption{versionpapier}{\versionenlignefalse} 
\DeclareOption{versionenligne}{\versionenlignetrue}
\DeclareOption{sansminisommaire}{\minisommairefalse}
\DeclareOption{versionreport}{\@versionreporttrue}

\if@versionreport
\DeclareOption*{\PassOptionsToClass{\CurrentOption}{report}}
\else
\DeclareOption*{\PassOptionsToClass{mer\CurrentOption}{book}}
\fi
%
% Exécution des options
%
\ProcessOptions
%
\if@versionreport
\LoadClass[a4paper]{report}
\else
\LoadClass[a4paper,twoside]{book}
\fi



%-----------------------------------------------------------------------
\RequirePackage{FramateX}
%\RequirePackage{dialogue} %%% MD ajout du 24-08


%----------------------------------------------------------------------
%
% les petits paquets à charger
%
\RequirePackage[french]{babel}
\RequirePackage{csquotes}

% Polices
\RequirePackage{textcomp,relsize}
\RequirePackage{mflogo,bbm,pifont}
%\RequirePackage{fouriernc}
\RequirePackage{times}
\RequirePackage{helvet}
%\RequirePackage{bookman} %%% DM 08-12
\RequirePackage[T1]{fontenc}
\RequirePackage[utf8]{inputenc}

% caractères
%\RequirePackage{xspace}	%espace
\RequirePackage{soul}	%Souligner

% base 
\RequirePackage{enumerate}	%listes

% graphics 
\RequirePackage{picins}	%figure dans un paragraphe
\RequirePackage{subfigure}	%subfigure dans figure
\RequirePackage{wrapfig}
\RequirePackage{xcolor}
\RequirePackage{graphicx}
\RequirePackage{framed} 
\RequirePackage{tikz} % tikz est utilise pour tracer les \boitemagique

% les tableaux
\RequirePackage{array,multirow,colortbl,longtable,multicol}

% légendes
\RequirePackage[labelsep=none]{caption}

% Math
\RequirePackage{amsmath,mathrsfs}

% Index
\RequirePackage{makeidx,index}

% Outils
\RequirePackage{calc}
\RequirePackage{ifthen}
\RequirePackage{float} 
\RequirePackage{float} 




% reinitialiser le compteur de notes de bas de page a chaque page
\RequirePackage[perpage]{footmisc}

%-----------------------------------------------------------------------
%-----------------------------------------------------------------------
\definecolor{fondtitre}{RGB}{130,130,130}
\definecolor{fonddeboite}{RGB}{232,232,232}
\definecolor{shadecolor}{RGB}{232,232,232}




%-----------------------------------------------------------------------
%
% hyperref qui pète les ouilles
%
\def\Hy@WarningNoLine#1{}
\def\Hy@Warning#1{}
%-----------------------------------------------------------------------

%%%%%%%%%% Boitemagique
\newcommand*{\boitemagique}[2]{
\begin{center}
\begin{tikzpicture}
% la boite
\node[rectangle,draw=fondtitre!100,fill=fonddeboite!100,inner sep=10pt, inner ysep=20pt] (mabox)
{
\begin{minipage}{12cm}
#2
\end{minipage}
};
% le titre de la boite
\node[fill=fondtitre!100, text=white, rectangle] at (mabox.north) {\sffamily\textbf{#1}};
\end{tikzpicture}
\end{center}
}

%%%%%% Encart %%%%%
\newcommand*{\boitesimple}[1]{%
\begin{shaded}
	#1
\end{shaded}
}



%---------------------------------------------------------------------
%
% pour changer les marges localement (merci Marie Paul ;-)
%
%----------------------------------------------------------------------

\newenvironment{changemargin}[2]{\begin{list}{}{%
\setlength{\leftmargin}{0pt}%
\setlength{\rightmargin}{0pt}%
\setlength{\listparindent}{\parindent}%
\setlength{\itemindent}{\parindent}%
\addtolength{\leftmargin}{#1}%
\addtolength{\rightmargin}{#2}%
}\item }{\end{list}}

\newenvironment{agrandirmarges}[2]{%
  \begin{list}{}{%
      \setlength{\topsep}{0pt}%
      \setlength{\listparindent}{\parindent}%
      \setlength{\itemindent}{\parindent}%
      \setlength{\parsep}{0pt plus 1pt}%
      \checkoddpage%
      \ifcpoddpage
        \setlength{\leftmargin}{-#1}\setlength{\rightmargin}{-#2}
      \else
        \setlength{\leftmargin}{-#2}\setlength{\rightmargin}{-#1}
      \fi}\item }%
  {\end{list}}


%-----------------------------------------------------------------------

%-----------------------------------------------------------------------
%-----------------------------------------------------------------------
\AtEndDocument{%
  }


%
% fin du fichier
%
\endinput
